<?php
// Heading
$_['heading_title']     = 'Cargus Ship And Go';

$_['text_cargus_index'] = 'Cargus';
$_['text_cargus_comanda'] = 'Comanda Curenta';
$_['text_cargus_istoric'] = 'Istoric Comenzi';
$_['text_cargus_preferinte'] = 'Preferinte';
$_['text_cargus_setari'] = 'Configurare';
$_['text_cargus_button_add_awb'] = 'Generare AWB';
$_['text_cargus_already_awb'] = 'Comanda este in lista de livrari cargus';
$_['text_cargus_error_new_awb'] = 'Eroare creare awb nou';
$_['text_cargus_print_awb'] = 'Print AWB';
$_['add_to_cargus'] = 'Adauga in lista de livrari Cargus';
$_['text_cargus_ship_and_go_index'] = 'Cargus Ship and Go';
$_['text_cargus_ship_and_go_preferinte'] = 'Preferinte';
