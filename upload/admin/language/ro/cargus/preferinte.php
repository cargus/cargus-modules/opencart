<?php
// Heading
$_['heading_title']             = 'Cargus - Preferinte';
$_['text_edit']                 = 'Editare metoda de livrare Cargus';
// Text
$_['text_shipping']             = 'Livrare';
$_['text_success']              = 'Succes: Ati modificat preferintele metodei de livrare Cargus!';
$_['text_choose_price']         = 'Alegeti un plan tarifar';
$_['text_choose_pickup']        = 'Alegeti un punct de lucru';
$_['text_no']                   = 'Nu';
$_['text_yes']                  = 'Da';
$_['text_cash']                 = 'Numerar';
$_['text_bank']                 = 'In cont';
$_['text_sender']               = 'Expeditor';
$_['text_recipient']            = 'Destinatar';
$_['text_parcel']               = 'Colet';
$_['text_envelope']             = 'Plic';
$_['text_error']                = 'Atentie:';
$_['button_cancel']             = 'Anulare';
// Entry
$_['entry_price']               = 'Plan tarifar';
$_['entry_pickup']              = 'Punct de lucru';
$_['entry_insurance']           = 'Asigurare';
$_['entry_saturday']            = 'Livrare sambata';
$_['entry_morning']             = 'Livrare dimineata';
$_['entry_openpackage']         = 'Deschidere colet';
$_['entry_repayment']           = 'Ramburs';
$_['entry_payer']               = 'Platitor expeditie';
$_['entry_type']                = 'Tip expeditie';
$_['entry_noextrakm']           = 'Ridicare din depozit Cargus';
$_['entry_noextrakm_details']   = 'Daca localitatea destinatarului are km suplimentari, oferiti posibilitatea ridicarii din cel mai apropiat depozit Urgent Cargus.';
$_['entry_free']                = 'Transport gratuit peste';
$_['entry_free_details']        = 'Livrarea standard este gratuita dar nu este inclus si costul kilometrilor suplimentari.';
$_['entry_fixed']               = 'Pret fix transport';
$_['entry_fixed_details']       = 'Costul transportului este fix, indiferent daca exista kilometri suplimentari.';
$_['entry_service_id']          = 'ID Serviciu';
$_['entry_service_id_details']  = 'ID Serviciu se trimite ca parametru in API de AWB';
$_['entry_service_id_1']        = '1 Standard';
$_['entry_service_id_34']       = '34 Economic Standard';
$_['entry_service_id_39']       = '39 Multipiece';

$_['entry_postal_codes']        = 'Nomenclator strazi/coduri postale';
$_['entry_postal_codes_details'] = 'Completeaza automat codul postal in baza adresei introduse';

$_['entry_awb_retur']           = 'Retur cumparator';
$_['entry_awb_retur_options_0']  = 'Nu';
$_['entry_awb_retur_options_1']  = 'Cod retur';
$_['entry_awb_retur_options_2']  = 'AWB Pre-tiparit';


$_['entry_print_awb_retur']     = 'Print AWB retur';

$_['entry_print_awb_retur_options_0'] = 'Nu';
$_['entry_print_awb_retur_options_2'] = 'Da cu instructiuni';

$_['entry_awb_retur_validitate'] = 'Validitate (zile)';
$_['entry_awb_retur_validitate_error'] = 'Validitate (zile) trebuie sa fie un numar intreg intre 0 si 180';

$_['entry_package_content_text'] = 'Text predefinit pentru continutul pachetului';
$_['entry_package_content_text_tooltip'] = 'Daca acest camp este gol atunci continutul cosului de cumparaturi este trimis ca text in continutul pachetului.<br>'.
                                           'Daca clientul are nevoie de confidentialitate, introduceti un text personalizat aici.';


// Error
$_['error_permission']          = 'Atentie: Nu aveti permisiunea de a edita preferintele metodei de livrare Cargus!';