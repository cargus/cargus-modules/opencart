<?php
// Heading
$_['heading_title']             = 'Cargus Ship and Go - Preferences';
$_['text_edit']                 = 'Edit Cargus Ship and Go method';
// Text
$_['text_shipping']             = 'Shipping';
$_['text_success']              = 'Success: You have modified Cargus ship and Go method preferences!';
$_['text_no']                   = 'No';
$_['text_yes']                  = 'Yes';
$_['text_error']                = 'Warning:';
$_['button_cancel']             = 'Cancel';
// Entry

$_['entry_free']                = 'Free shipping after';
$_['entry_free_details']        = 'Standard shipping is free but does not include the extra kilometers cost.';
$_['entry_fixed']               = 'Fixed shipping price';
$_['entry_fixed_details']       = 'Standard shipping will have a fixed cost, regardless of the extra kilometers.';
$_['entry_status']               = 'Status';
$_['entry_status_details']       = 'Enabling or disabling module.';
// Error
$_['error_permission']          = 'Warning: You do not have permission to modify Cargus Ship and Go method preferences!';