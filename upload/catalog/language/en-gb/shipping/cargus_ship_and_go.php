<?php
$_['text_title'] = 'Courier';
$_['text_description'] = 'Delivery with Cargus Ship & Go';
$_['text_description_2'] = 'Pickup from the closest Cargus warehouse';
$_['text_discount_urgent'] = 'Order value promotion';